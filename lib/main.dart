import 'package:flutter/material.dart';
import 'package:reg/screen/Login.dart';
import 'package:reg/screen/home.dart';

import 'package:reg/screen/timetable.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Login',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
        ),
        home: HomeScreen());
  }
}
