import 'package:flutter/material.dart';
import 'package:flutter_image_slider/carousel.dart';
import 'package:flutter_image_slider/indicator/Circle.dart';
import 'package:carousel_slider/carousel_slider.dart';

class CalendarScreen extends StatefulWidget {
  @override
  _CalendarScreen createState() => _CalendarScreen();
}

class _CalendarScreen extends State<CalendarScreen> {
  List<String> _imgList = [
    'assets/images/รูปภาพ1.png',
    'assets/images/รูปภาพ2.png',
    'assets/images/รูปภาพ3.png'
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('ประกาศเรื่อง'),
          backgroundColor: Colors.indigo,
        ),
        body: Center(
          child: Container(
            child: CarouselSlider(
              options: CarouselOptions(
                height: double.infinity,
                autoPlayAnimationDuration: Duration(milliseconds: 1000),
                aspectRatio: 1.873,
                viewportFraction: 1.0,
                autoPlay: true,
              ),
              items: _imgList
                  .map((item) => Image.asset(
                        item,
                        fit: BoxFit.fill,
                        width: double.infinity,
                      ))
                  .toList(),
            ),
          ),
        ),
      ),
    );
  }
}
