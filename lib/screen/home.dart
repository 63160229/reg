import 'package:flutter/material.dart';

import 'package:reg/screen/Login.dart';
import 'package:reg/screen/announces.dart';

import 'package:reg/screen/timetable.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Login"),
        backgroundColor: Colors.indigo,
      ),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 50, 10, 0),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Image.asset(
                "assets/images/Buu-logo11.png",
                width: 200,
                height: 200,
              ),
              const SizedBox(height: 10.0),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton.icon(
                    icon: Icon(Icons.login),
                    label: Text("เข้าสู่ระบบ", style: TextStyle(fontSize: 20)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return LoginScreen();
                        }),
                      );
                    }),
              ),
              const SizedBox(height: 10.0),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton.icon(
                    icon: Icon(Icons.schedule),
                    label:
                        Text("ตารางเรียนนิสิต", style: TextStyle(fontSize: 20)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return TimetableScreen();
                        }),
                      );
                    }),
              ),
              const SizedBox(height: 10.0),
              SizedBox(
                width: double.infinity,
                child: ElevatedButton.icon(
                    icon: Icon(Icons.speaker_notes),
                    label: Text("ประกาศเรื่อง", style: TextStyle(fontSize: 20)),
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return CalendarScreen();
                        }),
                      );
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
